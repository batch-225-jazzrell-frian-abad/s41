const Course = require('../models/course');


module.exports.addCourse = (data) => {
  if(data.isAdmin){
    let new_course = new Course ({
      name: data.course.name,
      description: data.course.description,
      price: data.course.price
    })

    return new_course.save().then((new_course, error) => {
      if(error){
        return error
      }
      return {
        message: 'New course successfully created!'
      }
    })
  } 

  let message = Promise.resolve('User must be Admin to Access this.')

  return message.then((value) => {
    return value

  // you can use this to simplify code
  // Promise.resolve('User must me Admin to Access this.')
  })

}



// ACTIVITY s40

// Controller function that retrieving all the courses

module.exports.getAllCourses = () => {
  return Course.find({}).then(result => {
    return result;
  });
};


module.exports.updateCourse = (reqParams, reqBody) => {

  // specify the fields/properties of the document to be updated

  // to update data no need to put "new"
  let updatedcourse = {
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price
  };

  /*
      Syntax:

      findByIdAndUpdate(document ID, updatesToBeApplied)

  */

  return Course.findByIdAndUpdate(reqParams.courseId, updatedcourse).then((course, error) => {

    if (error) {

      return false;

    } else {

      return {
        message: "Course updated successfully"
      }

    };


  });

};



module.exports.archiveCourse = (reqParams) =>{

  let updateActiveField = {

    isActive: false
  };

  return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) =>{

    if (error){

      return false;

    } else {

      return {

        message: "Archiving Course succesfully"

      }

    };
  });
};


module.exports.unarchiveCourse = (reqParams) =>{

  let updateActiveField = {

    isActive: true
  };

  return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) =>{

    if (error){

      return false;

    } else {

      return {

        message: "Unarchiving Course succesfully"

      }

    };
  });
};


module.exports.getAllActive = () => {

  return Course.find({isActive : true}).then(result => {
      return result
  });

};











// ===========================================================================================
// TO ADD COURSE AFTER THE AUTH TOKEN IS VERIFIED
/*module.exports.addCourse = (req, res, next) => {
  const newCourse = new Course({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price
  });

  newCourse.save((err, course) => {
    if (err) {
      return res.status(500).send({ message: 'Error adding course' });
    } else {
      return res.status(200).send({ message: 'Course added successfully' });
    }
  });
};*/


