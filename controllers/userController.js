const User = require('../models/user');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Course = require('../models/course');




module.exports.checkEmailExist = (reqBody) => {

	return User.find({email : reqBody.email}).then(result => {

		// the findOne method returns a record if a match is found

		if (result.length > 1) {

			return {
				message : 'Duplicate User'
			}

		} else if (result.length > 0) {

			return {
				message: 'User was found'
			}

		} else {

			return {
				message: 'User not found'
			}
		}

	})
}



module.exports.registerUser = (reqBody) => {


	let newUser = new User({

		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	}) 

	return newUser.save().then((user, error) => {

		if (error) {

			return false;

		} else {

			return user

		};
	});
};


// User Authentication

/*
	Steps:
	1. Check the database if the user email exists 
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not.
*/

// ACTIVITY s39
module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {

		if (result == null) {

			return {
				message: "Not Found in our Database"
			}

		} else {

			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password); 

			if (isPasswordCorrect) {

				return {access: auth.createAccessToken(result)}

			} else {

				// if password doesn't match
				return {
					message : "Password was Incorrect"
				}
			};
		};

	});
};


// Activity 40

module.exports.enroll = async (data) =>{

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId : data.courseId})


		return user.save().then((user, error) => {
			if (error){
				return false;
			} else {
				return true;
			};

		});
	});


	// Add the user ID in the enrollees array of the course
	// Using the "await" keyword will allow the enroll method to complete updating the course before returning a response back to the frontend
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		// Adds the userId in the course's enrollees array
		course.enrollees.push({userId : data.userId});


		// Saves the updated course information in the database
		return course.save().then((course, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});

	});

	// Condition that will check if the user and course documents have been updated
	// User enrollment successful
	if(isUserUpdated && isCourseUpdated){
		return {
			message: "You're Enrolled"
		}
	// User enrollment failure
	} else {
		return false;
	};
};












// ===================================================

// Activity s38

/*module.exports.getProfile = (userID, newContent) => {

	return User.findById(userID).then((result, err) => {

		if (err){
			console.log(err)
			return false;
		} 
		else {
			
			result.password = "  ";

			return result.save().then((updated, saveErr) => {
				if (saveErr) {
					console.log(saveErr);
					return false;
				} else{
					return updated;
				}
			})
		}
	})
}*/
// ACTIVITY ANSWER s38
module.exports.getProfile = (userID, newContent) => {

	return User.findById(userID).then(result => {

		result.password = "";

		return result;
		
	})
}

module.exports.getUser = () => {

	return User.find({}).then(result => {
		return result;
		
	})
}
