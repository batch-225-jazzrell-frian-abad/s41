const express = require('express');
const router = express.Router();
const auth = require('../auth');

const userController = require('../controllers/userController')

// Check Email 
router.post('/checkEmail', (req, res) => {
	userController.checkEmailExist(req.body).then(result => res.send(result));
});

// Route for registration

router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result));

});

// Activity s38

router.post('/details', (req, res) => {
	userController.getProfile(req.body).then(result => res.send(result));
});

// // ACTIVITY s39
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result));
})


// Activity 40
// Route for enrolling an authenticated user
// enrollments for the user

router.post('/enroll', auth.verify, (req, res) => {

	let data = {

		// user ID will be retrieved from the request header
		userId : auth.decode(req.headers.authorization).id,

		// Course ID will be retrieved from the request body
		courseId : req.body.courseId

	}

	userController.enroll(data).then(result => res.send(result));

	
});











router.get('/getUser', (req, res) => {
	userController.getUser().then(result => res.send(result));
});
module.exports = router;