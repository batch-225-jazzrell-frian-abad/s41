const express = require('express');
const router = express.Router();
const auth = require('../auth'); //used to verify the bearer token first
const courseController = require('../controllers/courseController');





// Router for creating a course

router.post('/addCourse', auth.verify, (req, res) => {

	const data = {
		course : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(result => res.send(result))
})



// ACTIVITY s40
router.get('/getCourse', (req, res) => {
    courseController.getAllCourses().then(result => 
        res.send(result));
});



// Router for updating the course

router.put('/:courseId', auth.verify, (req, res) => {

	// to check if the user isAdmin allow to update
	/*const decodedToken = auth.decode(req.headers.authorization);

	if (!decodedToken || !decodedToken.isAdmin) {
	    return res.status(401).send({ message: 'Unauthorized: Only admin users can update courses.' });
	}*/

	courseController.updateCourse(req.params, req.body).then( result => res.send(result));
});


// Routes for archiving 
router.put('/archive/:courseId', auth.verify, (req, res) =>{

	courseController.archiveCourse(req.params).then(result => res.send(result));
});


router.put('/unarchive/:courseId', auth.verify, (req, res) =>{

	courseController.unarchiveCourse(req.params).then(result => res.send(result));
});

router.get('/getActive', auth.verify, (req, res) => {
    courseController.getAllActive().then(result => 
        res.send(result));
});







/*
	Plan for capstone:

	Admins - Manage - (Product - All details - Active and Inactive

	Users - Buyer - (Product - limited details - active only

*/





// ==================================================================

// Route to add a new course for Admin User
// This will verify first the Auth bearer token and show failed if it's not a match then will allow you to input in the body if it's a match but will show failed to add course when no input is found.

//router.post('/add-course', auth.verify, courseController.addCourse);

//router.post('/add-course', auth.verify, auth.decode, courseController.addCourse);

//router.post('/add-course', auth.verify, auth.decode, auth.isAdmin, courseController.addCourse);







module.exports = router;